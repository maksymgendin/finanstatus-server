package com.finanstatus.server.service;

import java.io.Serializable;

import com.finanstatus.server.repository.GenericRepository;

public interface GenericService<T, P extends Serializable> extends GenericRepository<T, P> {
}