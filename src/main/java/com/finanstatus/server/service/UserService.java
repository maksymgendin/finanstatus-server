package com.finanstatus.server.service;

import java.util.Optional;

import com.finanstatus.server.domain.SimpleGrantedAuthority;
import com.finanstatus.server.domain.User;

public interface UserService extends GenericService<User, Long> {

	Optional<User> getCurrentUser();

	void createNewUser(User user, SimpleGrantedAuthority... authorities);

	Optional<User> findByUsername(String username);
}