package com.finanstatus.server.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface ExtendedUserDetailsService extends UserDetailsService {

	boolean isCurrentUserAdmin();

	String getCurrentUserName();
}