package com.finanstatus.server.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.finanstatus.server.domain.User;
import com.finanstatus.server.repository.UserRepository;

@Service("userDetailsService")
public class ExtendedUserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final Optional<User> user = userRepository.findByUsername(username);
		if (user.isPresent()) {
			if (user.get().getAuthorities() == null || user.get().getAuthorities().isEmpty()) {
				throw new UsernameNotFoundException("User '" + username + "' has no authorities.");
			}
		} else {
			throw new UsernameNotFoundException("User '" + username + "' was not found.");
		}
		return user.get();
	}
}