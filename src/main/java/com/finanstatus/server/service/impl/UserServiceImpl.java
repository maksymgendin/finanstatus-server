package com.finanstatus.server.service.impl;

import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.finanstatus.server.domain.SimpleGrantedAuthority;
import com.finanstatus.server.domain.User;
import com.finanstatus.server.repository.UserRepository;
import com.finanstatus.server.service.UserService;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, Long> implements UserService {

	private final UserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	public UserServiceImpl(final UserRepository userRepository) {
		super(userRepository);
		this.userRepository = userRepository;
	}

	@Override
	@Transactional
	public void createNewUser(final User user, final SimpleGrantedAuthority... authorities) {
		if (ArrayUtils.isNotEmpty(authorities)) {
			for (final SimpleGrantedAuthority authority : authorities) {
				user.getAuthorities().add(authority);
			}
		}
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		persist(user);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<User> getCurrentUser() {
		Optional<User> result = Optional.empty();
		final Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (user instanceof User) {
			result = Optional.ofNullable(find(((User) user).getId()));
		}
		return result;
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<User> findByUsername(final String username) {
		return userRepository.findByUsername(username);
	}
}