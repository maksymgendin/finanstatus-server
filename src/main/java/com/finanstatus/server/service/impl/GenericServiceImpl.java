package com.finanstatus.server.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.finanstatus.server.repository.GenericRepository;
import com.finanstatus.server.service.GenericService;

public class GenericServiceImpl<T, P extends Serializable> implements GenericService<T, P> {

	private final GenericRepository<T, P> entityRepository;

	protected GenericServiceImpl(final GenericRepository<T, P> entityRepository) {
		this.entityRepository = entityRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public T find(final P id) {
		return entityRepository.find(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> findAll() {
		return entityRepository.findAll();
	}

	@Override
	@Transactional
	public void persist(final T user) {
		entityRepository.persist(user);
	}

	@Override
	@Transactional
	public T merge(final T user) {
		return entityRepository.merge(user);
	}

	@Override
	@Transactional
	public void remove(final T user) {
		entityRepository.remove(user);
	}

	@Override
	@Transactional(readOnly = true)
	public void refresh(final T user) {
		entityRepository.refresh(user);
	}

	@Override
	@Transactional
	public void removeAllWithoutCascade() {
		entityRepository.removeAllWithoutCascade();
	}
}