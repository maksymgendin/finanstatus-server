package com.finanstatus.server.repository;

import com.finanstatus.server.domain.Account;

public interface AccountRepository extends GenericRepository<Account, Long> {
}