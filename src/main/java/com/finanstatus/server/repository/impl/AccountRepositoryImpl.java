package com.finanstatus.server.repository.impl;

import org.springframework.stereotype.Repository;

import com.finanstatus.server.domain.Account;
import com.finanstatus.server.repository.AccountRepository;

@Repository
public class AccountRepositoryImpl extends GenericRepositoryJpaImpl<Account, Long> implements AccountRepository {

	public AccountRepositoryImpl() {
		super(Account.class);
	}
}