package com.finanstatus.server.repository.impl;

import org.springframework.stereotype.Repository;

import com.finanstatus.server.domain.TransactionPeriodicity;
import com.finanstatus.server.repository.TransactionPeriodicityRepository;

@Repository
public class TransactionPeriodicityRepositoryImpl extends GenericRepositoryJpaImpl<TransactionPeriodicity, Long>
		implements TransactionPeriodicityRepository {

	public TransactionPeriodicityRepositoryImpl() {
		super(TransactionPeriodicity.class);
	}
}