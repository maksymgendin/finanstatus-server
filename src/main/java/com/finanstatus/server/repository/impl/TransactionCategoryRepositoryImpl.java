package com.finanstatus.server.repository.impl;

import org.springframework.stereotype.Repository;

import com.finanstatus.server.domain.TransactionCategory;
import com.finanstatus.server.repository.TransactionCategoryRepository;

@Repository
public class TransactionCategoryRepositoryImpl extends GenericRepositoryJpaImpl<TransactionCategory, Long> implements TransactionCategoryRepository {

	public TransactionCategoryRepositoryImpl() {
		super(TransactionCategory.class);
	}
}