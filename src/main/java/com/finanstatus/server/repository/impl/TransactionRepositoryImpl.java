package com.finanstatus.server.repository.impl;

import org.springframework.stereotype.Repository;

import com.finanstatus.server.domain.Transaction;
import com.finanstatus.server.repository.TransactionRepository;

@Repository
public class TransactionRepositoryImpl extends GenericRepositoryJpaImpl<Transaction, Long>
		implements TransactionRepository {

	public TransactionRepositoryImpl() {
		super(Transaction.class);
	}
}