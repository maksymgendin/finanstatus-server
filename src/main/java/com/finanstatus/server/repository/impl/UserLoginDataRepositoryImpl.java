package com.finanstatus.server.repository.impl;

import org.springframework.stereotype.Repository;

import com.finanstatus.server.domain.UserLoginData;
import com.finanstatus.server.repository.UserLoginDataRepository;

@Repository
public class UserLoginDataRepositoryImpl extends GenericRepositoryJpaImpl<UserLoginData, Long> implements UserLoginDataRepository {

	public UserLoginDataRepositoryImpl() {
		super(UserLoginData.class);
	}
}