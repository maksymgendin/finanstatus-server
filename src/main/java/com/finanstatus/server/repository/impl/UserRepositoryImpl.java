package com.finanstatus.server.repository.impl;

import java.util.Optional;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.finanstatus.server.domain.User;
import com.finanstatus.server.domain.UserLoginData_;
import com.finanstatus.server.domain.User_;
import com.finanstatus.server.repository.UserRepository;

@Repository
public class UserRepositoryImpl extends GenericRepositoryJpaImpl<User, Long> implements UserRepository {

	public UserRepositoryImpl() {
		super(User.class);
	}

	@Override
	public Optional<User> findByUsername(final String username) {
		User result = null;
		final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		final CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		final Root<User> root = criteriaQuery.from(entityClass);
		criteriaQuery.select(root);
		criteriaQuery.where(criteriaBuilder.equal(root.get(User_.loginData).get(UserLoginData_.username), username));
		final Query query = entityManager.createQuery(criteriaQuery);
		try {
			result = (User) query.getSingleResult();
		} catch (final PersistenceException exception) {
			// ignore the exception on this place because it is already handled by the framework
		}
		return Optional.ofNullable(result);
	}
}