package com.finanstatus.server.repository.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.repository.NoRepositoryBean;

import com.finanstatus.server.repository.GenericRepository;

@NoRepositoryBean
public class GenericRepositoryJpaImpl<T, P extends Serializable> implements GenericRepository<T, P> {

	@PersistenceContext
	protected transient EntityManager entityManager;

	protected Class<T> entityClass;

	protected GenericRepositoryJpaImpl(final Class<T> clazz) {
		this.entityClass = clazz;
	}

	@Override
	public T find(final P id) {
		final T entity = entityManager.find(entityClass, id);
		if (entity != null) {
			return entity;
		}
		throw new EntityNotFoundException();
	}

	@Override
	public List<T> findAll() {
		final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		final CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		final Root<T> rootEntry = criteriaQuery.from(entityClass);
		final CriteriaQuery<T> all = criteriaQuery.select(rootEntry);
		final TypedQuery<T> allQuery = entityManager.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public T merge(final T entity) {
		return entityManager.merge(entity);
	}

	@Override
	public void remove(final T entity) {
		entityManager.remove(entityManager.merge(entity));
	}

	@Override
	public void refresh(final T entity) {
		entityManager.refresh(entity);
	}

	@Override
	public void removeAllWithoutCascade() {
		entityManager.createQuery("delete from " + entityClass.getSimpleName()).executeUpdate();
	}

	@Override
	public void persist(final T entity) {
		entityManager.persist(entity);
	}
}