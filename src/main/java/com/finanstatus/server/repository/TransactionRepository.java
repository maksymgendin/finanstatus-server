package com.finanstatus.server.repository;

import com.finanstatus.server.domain.Transaction;

public interface TransactionRepository extends GenericRepository<Transaction, Long> {
}