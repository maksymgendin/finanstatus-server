package com.finanstatus.server.repository;

import com.finanstatus.server.domain.TransactionPeriodicity;

public interface TransactionPeriodicityRepository extends GenericRepository<TransactionPeriodicity, Long> {
}