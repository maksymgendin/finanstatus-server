package com.finanstatus.server.repository.converter;

import java.time.ZoneOffset;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ZoneOffsetPersistenceConverter implements AttributeConverter<ZoneOffset, Integer> {

	@Override
	public Integer convertToDatabaseColumn(final ZoneOffset entityValue) {
		Integer result = null;
		if (entityValue != null) {
			result = entityValue.getTotalSeconds();
		}
		return result;
	}

	@Override
	public ZoneOffset convertToEntityAttribute(final Integer databaseValue) {
		ZoneOffset result = null;
		if (databaseValue != null) {
			result = ZoneOffset.ofTotalSeconds(databaseValue);
		}
		return result;
	}
}