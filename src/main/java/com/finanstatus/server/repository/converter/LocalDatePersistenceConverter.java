package com.finanstatus.server.repository.converter;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LocalDatePersistenceConverter implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(final LocalDate entityValue) {
		Date result = null;
		if (entityValue != null) {
			result = Date.valueOf(entityValue);
		}
		return result;
	}

	@Override
	public LocalDate convertToEntityAttribute(final Date databaseValue) {
		LocalDate result = null;
		if (databaseValue != null) {
			result = databaseValue.toLocalDate();
		}
		return result;
	}
}