package com.finanstatus.server.repository.converter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LocalDateTimePersistenceConverter implements AttributeConverter<LocalDateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(final LocalDateTime entityValue) {
		Timestamp result = null;
		if (entityValue != null) {
			result = Timestamp.valueOf(entityValue);
		}
		return result;
	}

	@Override
	public LocalDateTime convertToEntityAttribute(final Timestamp databaseValue) {
		LocalDateTime result = null;
		if (databaseValue != null) {
			result = databaseValue.toLocalDateTime();
		}
		return result;
	}
}