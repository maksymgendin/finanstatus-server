package com.finanstatus.server.repository;

import java.io.Serializable;
import java.util.List;

public interface GenericRepository<T, P extends Serializable> {

	T find(P id);

	List<T> findAll();

	void persist(T entity);

	T merge(T entity);

	void remove(T entity);

	void refresh(T entity);

	void removeAllWithoutCascade();
}