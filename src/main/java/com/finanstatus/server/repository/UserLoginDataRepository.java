package com.finanstatus.server.repository;

import com.finanstatus.server.domain.UserLoginData;

public interface UserLoginDataRepository extends GenericRepository<UserLoginData, Long> {
}