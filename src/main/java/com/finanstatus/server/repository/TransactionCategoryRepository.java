package com.finanstatus.server.repository;

import com.finanstatus.server.domain.TransactionCategory;

public interface TransactionCategoryRepository extends GenericRepository<TransactionCategory, Long> {
}