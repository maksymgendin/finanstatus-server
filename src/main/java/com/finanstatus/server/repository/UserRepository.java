package com.finanstatus.server.repository;

import java.util.Optional;

import com.finanstatus.server.domain.User;

public interface UserRepository extends GenericRepository<User, Long> {

	Optional<User> findByUsername(String username);
}