package com.finanstatus.server.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Profile;

import com.finanstatus.server.aop.logging.LoggingAspect;
import com.finanstatus.server.configuration.profile.Profiles;

@Configuration
@EnableAspectJAutoProxy
public class SpringLoggingAspectConfiguration {

	@Bean
	@Profile({ Profiles.DEV_H2, Profiles.DEV_POSTGRE, Profiles.TEST })
	public LoggingAspect loggingAspect() {
		return new LoggingAspect();
	}
}