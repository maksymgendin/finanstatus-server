package com.finanstatus.server.configuration.profile;

public final class Profiles {

	public static final String TEST = "test";
	public static final String DEV_H2 = "dev-h2";
	public static final String DEV_POSTGRE = "dev-postgre";
	public static final String PROD = "prod";

	private Profiles() {
	}
}