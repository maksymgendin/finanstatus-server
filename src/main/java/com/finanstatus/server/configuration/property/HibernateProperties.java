package com.finanstatus.server.configuration.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "hibernate")
public class HibernateProperties {

	private String dialect;
	private String showSql;
	private String formatSql;
	private String hbm2ddlImportFiles;
	private String hbm2ddlAuto;

	public String getDialect() {
		return dialect;
	}

	public void setDialect(final String dialect) {
		this.dialect = dialect;
	}

	public String getShowSql() {
		return showSql;
	}

	public void setShowSql(final String showSql) {
		this.showSql = showSql;
	}

	public String getFormatSql() {
		return formatSql;
	}

	public void setFormatSql(final String formatSql) {
		this.formatSql = formatSql;
	}

	public String getHbm2ddlImportFiles() {
		return hbm2ddlImportFiles;
	}

	public void setHbm2ddlImportFiles(final String hbm2ddlImportFiles) {
		this.hbm2ddlImportFiles = hbm2ddlImportFiles;
	}

	public String getHbm2ddlAuto() {
		return hbm2ddlAuto;
	}

	public void setHbm2ddlAuto(final String hbm2ddlAuto) {
		this.hbm2ddlAuto = hbm2ddlAuto;
	}
}