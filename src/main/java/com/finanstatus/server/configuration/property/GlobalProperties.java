package com.finanstatus.server.configuration.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "global")
public class GlobalProperties {

	private String packagesToScan;

	public String getPackagesToScan() {
		return packagesToScan;
	}

	public void setPackagesToScan(final String packagesToScan) {
		this.packagesToScan = packagesToScan;
	}
}