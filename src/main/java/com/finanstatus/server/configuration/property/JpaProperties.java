package com.finanstatus.server.configuration.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "jpa")
public class JpaProperties {

	private String persistenceUnitName;

	public String getPersistenceUnitName() {
		return persistenceUnitName;
	}

	public void setPersistenceUnitName(final String persistenceUnitName) {
		this.persistenceUnitName = persistenceUnitName;
	}
}