package com.finanstatus.server.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.support.TransactionTemplate;

import com.finanstatus.server.configuration.property.GlobalProperties;
import com.finanstatus.server.configuration.property.HibernateProperties;
import com.finanstatus.server.configuration.property.JpaProperties;

@Configuration
public class SpringInfrastructureConfiguration {

	@Autowired
	private GlobalProperties globalProperties;
	@Autowired
	private HibernateProperties hibernateProperties;
	@Autowired
	private JpaProperties jpaProperties;
	@Autowired
	private DataSource dataSource;

	@Bean
	public JpaTransactionManager transactionManager() {
		final JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory());
		return jpaTransactionManager;
	}

	@Bean
	public TransactionTemplate transactionTemplate() {
		final TransactionTemplate transactionTemplate = new TransactionTemplate();
		transactionTemplate.setTransactionManager(transactionManager());
		return transactionTemplate;
	}

	@Bean
	public EntityManagerFactory entityManagerFactory() {
		final LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource(dataSource);
		entityManagerFactory.setPersistenceUnitName(jpaProperties.getPersistenceUnitName());
		entityManagerFactory.setPackagesToScan(globalProperties.getPackagesToScan());
		entityManagerFactory.setJpaVendorAdapter(jpaVendorAdaper());
		entityManagerFactory.setJpaPropertyMap(additionalJpaProperties());
		entityManagerFactory.afterPropertiesSet();
		return entityManagerFactory.getObject();
	}

	@Bean
	public JpaVendorAdapter jpaVendorAdaper() {
		return new HibernateJpaVendorAdapter();
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	private Map<String, Object> additionalJpaProperties() {
		final Map<String, Object> properties = new HashMap<>();
		properties.put(Environment.DIALECT, hibernateProperties.getDialect());
		properties.put(Environment.SHOW_SQL, hibernateProperties.getShowSql());
		properties.put(Environment.FORMAT_SQL, hibernateProperties.getFormatSql());
		properties.put(Environment.HBM2DDL_AUTO, hibernateProperties.getHbm2ddlAuto());
		properties.put(Environment.HBM2DDL_IMPORT_FILES, hibernateProperties.getHbm2ddlImportFiles());
		return properties;
	}
}