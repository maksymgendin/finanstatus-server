package com.finanstatus.server.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = "${global.packagesToScan}")
@EnableTransactionManagement
@EnableSpringConfigured
@EnableAutoConfiguration(exclude = RepositoryRestMvcAutoConfiguration.class)
public class SpringMainConfiguration {

	public static void main(final String... args) {
		SpringApplication.run(SpringMainConfiguration.class, args);
	}
}