package com.finanstatus.server.excluded;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaExport.Action;
import org.hibernate.tool.schema.TargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchemaGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(SchemaGenerator.class);

	public static void main(final String... args) {
		final SchemaGenerator gen = new SchemaGenerator();
		try {
			gen.generate(Dialect.POSTGRESQL, args[0] + "/", Arrays.copyOfRange(args, 1, args.length));
		} catch (ClassNotFoundException exception) {
			LOG.error("Class not found.", exception);
		} finally {
			System.exit(0);
		}
	}

	private List<Class<?>> getClasses(final String packageName) throws ClassNotFoundException {
		final ClassLoader cld = getClassLoader();
		final URL resource = getResource(packageName, cld);
		final File directory = new File(resource.getFile());
		return collectClasses(packageName, directory);
	}

	private ClassLoader getClassLoader() throws ClassNotFoundException {
		final ClassLoader cld = Thread.currentThread().getContextClassLoader();
		if (cld == null) {
			throw new ClassNotFoundException("Can't get classloader.");
		}
		return cld;
	}

	private URL getResource(final String packageName, final ClassLoader cld) throws ClassNotFoundException {
		final String path = packageName.replace('.', '/');
		final URL resource = cld.getResource(path);
		if (resource == null) {
			throw new ClassNotFoundException("No resource for " + path);
		}
		return resource;
	}

	private List<Class<?>> collectClasses(final String packageName, final File directory)
			throws ClassNotFoundException {
		final List<Class<?>> classes = new ArrayList<>();
		if (directory.exists()) {
			final String[] files = directory.list();
			if (files != null) {
				for (final String file : files) {
					if (file.endsWith(".class")) {
						// removes the .class extension
						classes.add(Class.forName(packageName + '.' + file.substring(0, file.length() - 6)));
					}
				}
			}
		} else {
			throw new ClassNotFoundException(packageName + " is not a valid package");
		}
		return classes;
	}

	private void generate(final Dialect dialect, final String directory, final String... packagesName)
			throws ClassNotFoundException {
		final MetadataSources metadata = new MetadataSources(new StandardServiceRegistryBuilder()
				.applySetting("hibernate.dialect", dialect.getDialectClass()).build());

		for (final String packageName : packagesName) {
			for (final Class<?> clazz : getClasses(packageName)) {
				metadata.addAnnotatedClass(clazz);
			}
		}

		final SchemaExport export = new SchemaExport();

		export.setDelimiter(";");
		export.setOutputFile(directory + "ddl_" + dialect.name().toLowerCase(Locale.ENGLISH) + ".sql");
		export.setFormat(true);
		export.execute(EnumSet.of(TargetType.SCRIPT, TargetType.STDOUT), Action.BOTH, metadata.buildMetadata());
	}

	private static enum Dialect {
		H2("org.hibernate.dialect.H2Dialect"), POSTGRESQL("org.hibernate.dialect.PostgreSQLDialect");

		private String dialectClass;

		private Dialect(final String dialectClass) {
			this.dialectClass = dialectClass;
		}

		public String getDialectClass() {
			return dialectClass;
		}
	}
}