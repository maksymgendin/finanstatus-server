package com.finanstatus.server.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

@Entity
@Table(name = "FS_TRANSACTION_CATEGORY")
public class TransactionCategory extends GenericBean {

	@Transient
	private static final long serialVersionUID = 5655619348441524389L;

	@Column(name = "CATEGORY_NAME", nullable = false)
	private String categoryName;

	@OneToMany(mappedBy = "transactionCategory")
	private List<Transaction> transactions;

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(final String categoryName) {
		this.categoryName = categoryName;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toStringExclude(this, "transactions");
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		return super.equals(other);
	}
}