package com.finanstatus.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FS_TRANSACTION_PERIODICITY")
public class TransactionPeriodicity extends GenericBean {

	@Transient
	private static final long serialVersionUID = 6635297784510212847L;

	public static enum Type {
		NON_RECURRING, RECURRING;
	}

	public static enum Rate {
		DAILY, WEEKLY, MONTHLY, YEARLY;
	}

	@OneToOne(mappedBy = "periodicity")
	private Transaction transaction;

	@Column(name = "TYPE", nullable = false)
	@Enumerated
	private Type type;

	@Column(name = "RATE", nullable = false)
	@Enumerated
	private Rate rate;

	@Column(name = "RATE_FACTOR", nullable = false)
	private Integer rateFactor;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(final Transaction transaction) {
		this.transaction = transaction;
	}

	public Type getType() {
		return type;
	}

	public void setType(final Type type) {
		this.type = type;
	}

	public Rate getRate() {
		return rate;
	}

	public void setRate(final Rate rate) {
		this.rate = rate;
	}

	public Integer getRateFactor() {
		return rateFactor;
	}

	public void setRateFactor(final Integer rateFactor) {
		this.rateFactor = rateFactor;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		return super.equals(other);
	}
}