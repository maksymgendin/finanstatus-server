package com.finanstatus.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FS_TRANSACTION_OPPONENT")
public class TransactionOpponent extends GenericBean {

	@Transient
	private static final long serialVersionUID = 7641770671597924551L;

	public static enum Type {
		INTERNAL, EXTERNAL;
	}

	@OneToOne(mappedBy = "transactionOpponent")
	private Transaction transaction;

	@Column(name = "TYPE", nullable = false)
	@Enumerated
	private Type type;

	@Column(name = "NAME")
	private String name;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ACCOUNT_ID")
	private Account account;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(final Transaction transaction) {
		this.transaction = transaction;
	}

	public Type getType() {
		return type;
	}

	public void setType(final Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(final Account account) {
		this.account = account;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		return super.equals(other);
	}
}