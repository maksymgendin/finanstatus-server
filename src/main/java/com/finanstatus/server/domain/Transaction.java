package com.finanstatus.server.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.finanstatus.server.repository.converter.LocalDatePersistenceConverter;

@Entity
@Table(name = "FS_TRANSACTION")
public class Transaction extends GenericBean {

	@Transient
	private static final long serialVersionUID = -5361671436261123915L;

	@ManyToOne(optional = false)
	@JoinColumn(name = "USER_ID")
	private User user;

	@Column(name = "AMOUNT", nullable = false)
	private BigDecimal amount;

	@Column(name = "TRANSACTION_DATE", nullable = false)
	@Convert(converter = LocalDatePersistenceConverter.class)
	private LocalDate transactionDate;

	@Column(name = "END_DATE")
	@Convert(converter = LocalDatePersistenceConverter.class)
	private LocalDate endDate;

	@ManyToOne(optional = false)
	@JoinColumn(name = "CATEGORY_ID")
	private TransactionCategory transactionCategory;

	@ManyToOne(optional = false)
	@JoinColumn(name = "OPPONENT_ID")
	private TransactionOpponent transactionOpponent;

	@OneToOne(optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "PERIODICITY_ID")
	private TransactionPeriodicity periodicity;

	public boolean isIndefinite() {
		return endDate == null;
	}

	public User getUser() {
		return user;
	}

	public void setUser(final User user) {
		this.user = user;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(final BigDecimal amount) {
		this.amount = amount;
	}

	public LocalDate getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(final LocalDate transactionDate) {
		this.transactionDate = transactionDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(final LocalDate endDate) {
		this.endDate = endDate;
	}

	public TransactionCategory getTransactionCategory() {
		return transactionCategory;
	}

	public void setTransactionCategory(final TransactionCategory transactionCategory) {
		this.transactionCategory = transactionCategory;
	}

	public TransactionOpponent getTransactionOpponent() {
		return transactionOpponent;
	}

	public void setTransactionOpponent(final TransactionOpponent transactionOpponent) {
		this.transactionOpponent = transactionOpponent;
	}

	public TransactionPeriodicity getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(final TransactionPeriodicity periodicity) {
		this.periodicity = periodicity;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		return super.equals(other);
	}
}