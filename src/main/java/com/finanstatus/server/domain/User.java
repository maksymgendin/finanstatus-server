package com.finanstatus.server.domain;

import java.time.ZoneOffset;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.security.core.userdetails.UserDetails;

import com.finanstatus.server.repository.converter.ZoneOffsetPersistenceConverter;

@Entity
@Table(name = "FS_USER")
public class User extends GenericBean implements UserDetails {

	@Transient
	private static final long serialVersionUID = 5334961404930990030L;

	@Column(name = "EMAIL", unique = true)
	private String email;

	@Embedded
	private UserLoginData loginData;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "FS_USER_TO_AUTHORITY", joinColumns = {
			@JoinColumn(name = "USER_ID", referencedColumnName = "ID") }, uniqueConstraints = {
					@UniqueConstraint(columnNames = { "USER_ID", "AUTHORITY" }) })
	private Set<SimpleGrantedAuthority> authorities;

	@Column(name = "ZONE_OFFSET")
	@Convert(converter = ZoneOffsetPersistenceConverter.class)
	private ZoneOffset zoneOffset = ZoneOffset.UTC;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "user")
	private Set<Transaction> transactions;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public UserLoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(final UserLoginData loginData) {
		this.loginData = loginData;
	}

	@Override
	public Set<SimpleGrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(final Set<SimpleGrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return loginData.isEnabled();
	}

	@Override
	public boolean isAccountNonLocked() {
		return loginData.isEnabled();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return loginData.isEnabled();
	}

	@Override
	public boolean isEnabled() {
		return loginData.isEnabled();
	}

	@Override
	public String getPassword() {
		return loginData.getPassword();
	}

	@Override
	public String getUsername() {
		return loginData.getUsername();
	}

	public void setEnabled(final boolean enabled) {
		loginData.setEnabled(enabled);
	}

	public void setPassword(final String password) {
		loginData.setPassword(password);
	}

	public void setUsername(final String username) {
		loginData.setUsername(username);
	}

	public ZoneOffset getZoneOffset() {
		return zoneOffset;
	}

	public void setZoneOffset(final ZoneOffset zoneOffset) {
		this.zoneOffset = zoneOffset;
	}

	public Set<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(final Set<Transaction> transactions) {
		this.transactions = transactions;
	}

	@Override
	public int hashCode() {
		return Objects.hash(loginData);
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if (other != null && getClass() == other.getClass()) {
			return Objects.equals(loginData, ((User) other).loginData);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toStringExclude(this, "transactions");
	}
}