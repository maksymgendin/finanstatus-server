package com.finanstatus.server.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

@Embeddable
public class UserLoginData implements Serializable {

	@Transient
	private static final long serialVersionUID = 2446873639545907758L;

	@Column(name = "USERNAME", nullable = false, unique = true)
	private String username;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "ENABLED")
	private boolean enabled;

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public int hashCode() {
		return Objects.hash(username, password);
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if (other != null && getClass() == other.getClass()) {
			final UserLoginData userLoginData = (UserLoginData) other;
			return Objects.equals(username, userLoginData.username) && Objects.equals(password, userLoginData.password);
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toStringExclude(this, "password");
	}
}