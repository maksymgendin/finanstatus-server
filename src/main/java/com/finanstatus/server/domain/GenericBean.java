package com.finanstatus.server.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.finanstatus.server.repository.converter.LocalDateTimePersistenceConverter;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

@MappedSuperclass
public class GenericBean implements Serializable, Comparable<GenericBean> {

	@Transient
	private static final long serialVersionUID = 950640262639137131L;

	@Id
	@Column(name = "ID", nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column(name = "CREATED_ON", nullable = false, updatable = false)
	@Convert(converter = LocalDateTimePersistenceConverter.class)
	protected LocalDateTime createdOn;

	@ManyToOne
	@JoinColumn(name = "CREATED_BY")
	protected User createdBy;

	@Column(name = "UPDATED_ON", nullable = false)
	@Convert(converter = LocalDateTimePersistenceConverter.class)
	protected LocalDateTime updatedOn;

	@ManyToOne
	@JoinColumn(name = "UPDATED_BY")
	protected User updatedBy;

	@Column(name = "VERSION", nullable = false)
	@Version
	protected Long version;

	@PrePersist
	protected void onPrePersist() {
		createdOn = updatedOn = LocalDateTime.now();
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			final Object principal = authentication.getPrincipal();
			if (principal instanceof User) {
				createdBy = updatedBy = (User) principal;
			}
		}
	}

	@PreUpdate
	protected void onPreUpdate() {
		updatedOn = LocalDateTime.now();
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			final Object principal = authentication.getPrincipal();
			if (principal instanceof User) {
				updatedBy = (User) principal;
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(final LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(final LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(final Long version) {
		this.version = version;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(final User createdBy) {
		this.createdBy = createdBy;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(final User updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if (other != null && getClass() == other.getClass()) {
			return Objects.equals(id, ((GenericBean) other).id);
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(final GenericBean other) {
		return ComparisonChain.start().compare(id, other.id, Ordering.natural().nullsLast()).result();
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}