package com.finanstatus.server.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;

@Embeddable
public class SimpleGrantedAuthority implements GrantedAuthority {

	@Transient
	private static final long serialVersionUID = 7385106839352546807L;

	public static enum Authority {
		ROLE_ADMIN("ROLE_ADMIN"), ROLE_USER("ROLE_USER"), ROLE_ANONYMOUS("ROLE_ANONYMOUS");

		private final String authority;

		private Authority(final String authority) {
			this.authority = authority;
		}

		public String getAuthority() {
			return authority;
		}
	}

	@Column(name = "AUTHORITY", nullable = false)
	@Enumerated(EnumType.STRING)
	private Authority authorityEnum;

	public SimpleGrantedAuthority() {
		super();
	}

	public SimpleGrantedAuthority(final Authority authority) {
		this.authorityEnum = authority;
	}

	@Override
	public String getAuthority() {
		return authorityEnum.getAuthority();
	}

	public Authority getAuthorityEnum() {
		return authorityEnum;
	}

	public void setAuthorityEnum(final Authority authority) {
		this.authorityEnum = authority;
	}

	@Override
	public boolean equals(final Object other) {
		if (this == other) {
			return true;
		}
		if (other != null && getClass() == other.getClass()) {
			return authorityEnum == ((SimpleGrantedAuthority) other).authorityEnum;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(authorityEnum);
	}

	@Override
	public String toString() {
		return authorityEnum.getAuthority();
	}
}