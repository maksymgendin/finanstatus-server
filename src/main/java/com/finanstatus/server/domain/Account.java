package com.finanstatus.server.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "FS_ACCOUNT")
public class Account extends GenericBean {

	@Transient
	private static final long serialVersionUID = 8004399562219901077L;

	@Column(name = "BALANCE", nullable = false)
	private BigDecimal balance;

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(final BigDecimal balance) {
		this.balance = balance;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(final Object other) {
		return super.equals(other);
	}
}