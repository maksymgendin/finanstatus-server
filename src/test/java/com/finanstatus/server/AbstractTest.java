package com.finanstatus.server;

import java.util.HashSet;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.finanstatus.server.domain.User;
import com.finanstatus.server.domain.UserLoginData;

public abstract class AbstractTest extends AbstractSpringTestConfiguration {

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractTest.class);

	protected User createUser() {
		final String random = UUID.randomUUID().toString();
		return createUser(random, random);
	}

	protected User createUser(final String username, final String password) {
		final User user = new User();
		user.setLoginData(new UserLoginData());
		user.setAuthorities(new HashSet<>());
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}
}