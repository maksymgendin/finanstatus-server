package com.finanstatus.server.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithUserDetails;

import com.finanstatus.server.AbstractTest;
import com.finanstatus.server.domain.SimpleGrantedAuthority;
import com.finanstatus.server.domain.SimpleGrantedAuthority.Authority;
import com.finanstatus.server.domain.User;
import com.finanstatus.server.service.UserService;

public class UserServiceTest extends AbstractTest {

	@Autowired
	private UserService userService;

	@Test
	public void testCreateUserWithoutPrinciple() {
		final User user = createUser();
		userService.createNewUser(user, new SimpleGrantedAuthority(Authority.ROLE_USER));
		assertNull("'createdBy' should be null", user.getCreatedBy());
	}

	@Test
	@WithUserDetails("admin")
	public void testCreateUserWithPrinciple() {
		final User user = createUser();
		userService.createNewUser(user, new SimpleGrantedAuthority(Authority.ROLE_USER));
		assertEquals("username of 'createdBy' user should be 'admin'", user.getCreatedBy().getUsername(), "admin");
	}
}