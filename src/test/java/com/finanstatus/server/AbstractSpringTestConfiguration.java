package com.finanstatus.server;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.finanstatus.server.configuration.SpringMainConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainConfiguration.class)
@ActiveProfiles(profiles = "test")
public abstract class AbstractSpringTestConfiguration {
}